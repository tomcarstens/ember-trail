import Head from 'next/head';
import { useState } from 'react';
import SlidingPanel from 'react-sliding-side-panel';
import { Compendium } from '../components/compendium';
import { Terminal } from '../components/terminal';
import { Inventory } from '../components/inventory';
import { Item } from '../components/item';
import { Page } from '../components/page';
import { CompendiumProvider } from '../contexts/side-panel';
import { interpret } from '../interpreter/interpreter';
import { CharacterProvider } from '../contexts/character';
import { Stats } from '../components/stats';

const icons = require('../public/icons.json');

export default function Home() {
  return (
    <CharacterProvider>
      <CompendiumProvider>
        <Head>
          <title>Ember Trails Character Sheet</title>
        </Head>
        <div className="flow">
          <Page>
            <Terminal interpreter={interpret}></Terminal>
          </Page>
          <Page>
            <Stats></Stats>
            Inventory
            <Inventory></Inventory>

          </Page>
        </div>
      </CompendiumProvider>
    </CharacterProvider>
  )
}
