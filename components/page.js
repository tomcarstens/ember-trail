import styles from './page.module.css';
export function Page(props) {
    return <div className={styles.page}>{props.children}</div>;
}