import { useContext } from "react";
import { CharacterContext } from "../contexts/character";
import styles from './stats.module.css'

export function Stats(props) {
    const { character, update } = useContext(CharacterContext);
    return <div>
        <h1>{character.name}</h1>
        <label>HP {character.hp}/6</label>
        <div className={styles.progress}>
            <span className={styles.hp} style={{ width: (100 * character.hp / 6) + '%' }}>&nbsp;</span>
        </div>
        <label>MP  {character.mp}/6</label>
        <div className={styles.progress}>
            <span className={styles.mp} style={{ width: (100 * character.mp / 6) + '%' }}>&nbsp;</span>
        </div>
        <table className={styles.abilities}>
            <caption>Ability Scores</caption>
            <tr><th>Strength</th><th>Dexterity</th><th>Intelligence</th><th>Spirit</th></tr>
            <tr>
                <td>{character.strength}</td>
                <td>{character.dexterity}</td>
                <td>{character.intelligence}</td>
                <td>{character.spirit}</td>
            </tr>
        </table>

    </div>;
}