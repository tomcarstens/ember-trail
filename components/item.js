import { useContext } from "react";
import { CompendiumContext } from "../contexts/side-panel";
import styles from './item.module.css';
const icons = require('../public/icons.json');
export function Item(props) {
    const compendium = useContext(CompendiumContext);
    const icon  = icons.find(i => i.name === props.name);
    return icon ? 
    <div>
        <img src={icon.path} title={icon.name}></img>
    </div> :
    <div className={styles.full} onClick={()=>compendium.dispatch(true)}><span>+</span></div>;
}
