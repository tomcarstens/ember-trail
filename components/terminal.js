import { useState } from 'react';
import styles from './terminal.module.css';

export function Terminal(props) {
    const [log, setLog] = useState('Welcome to Ember Trails.\nAdventure Awaits...\n\n');
    const [value, setValue] = useState('');
    const updateLog = event => {
        const entry = event.target[0].value;
        setLog(
            [
                log,
                props.interpreter ?
                    props.interpreter(entry) :
                    entry
            ].join('\n\n'));
            setValue('');
        event.preventDefault();
    }
        
    return <div>
        <textarea className={styles.log} rows="52" value={log} readOnly></textarea>
            <form onSubmit={updateLog} className={styles.container}>
            <span>&gt;</span><input className={styles.entry} value={value} onChange={e => setValue(e.target.value)} /></form>
    </div>;
}
