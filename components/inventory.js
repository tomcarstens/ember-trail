import { useContext } from 'react';
import { CharacterContext } from '../contexts/character';
import { Item } from './item';
import styles from './inventory.module.css'
export function Inventory(props) {
    const {character:{inventory}, update} = useContext(CharacterContext);
    const rows = Array.from(Array(4)).map(
        (_, index) =>
            <tr key={index}>
                {Array.from(Array(3), (_,cell) => 
                    <td key={index+':'+cell} className={styles.cell}><Item name={inventory[cell+(index*3)]}></Item></td>)}
            </tr>
    
    )
    return <table><tbody>{rows}</tbody></table>;
}