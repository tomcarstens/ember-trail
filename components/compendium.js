import styles from './compendium.module.css';
const icons = require('../public/icons.json');
export function Compendium(props) {
    const items = Array.from(Array(40), (_, index) =>
        <img key={index} className={styles.item} src={icons[index].path} title={icons[index].name}></img>);
    return <>
        Item Compendium
        <div className={styles.compendium}>
            {items}
        </div>
    </>;
}