export function interpret(input, context) {
    const [cmd, ...args] = input.match(/("[^"]*")|\w+/g);
    console.log(cmd, JSON.stringify(args))
    switch(cmd) {
        case 'say':
            return args.map(arg => arg.replaceAll('"','')).join(' ');
        default:
            return `${cmd} not understood, try again. Or enter 'help' for a list of understood commands.`
    }
}