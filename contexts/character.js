import React, { useReducer } from "react";

const initialState = {
    name: 'Bob Ross',
    strength: 6,
    dexterity: 6,
    intelligence: 6,
    dexterity: 6,
    spirit: 6,
    hp: 4,
    mp: 6,
    inventory: [ 
        'Sword',
        'Wood',
        'Helm',
        'Armor',
        'Shoes'
    ]
}
export const CharacterContext = React.createContext({
    character: initialState,
    update: () => null
});

const reducer =  (state, action) => {
    switch(action.type) {
        case 'name':
            return {...state, name:action.value};
        case 'strength':
            return {...state, strength:action.value};
        case 'dexterity':
            return {...state, dexterity:action.value};
        case 'intelligence':
            return {...state, intelligence:action.value};
        case 'spirit':
            return {...state, spirit:action.value};
        case 'hp':
            return {...state, hp:action.value};
        case 'mp':
            return {...state, mp:action.value};
        case 'inventory':
            return {...state, inventory:action.value};
        default:
            return {...action.value};
    };
    
};;
export function CharacterProvider(props) {
    const [character, update] = useReducer(reducer, initialState)
    return <CharacterContext.Provider value={{character, update}}>
        {props.children}
    </CharacterContext.Provider>
}