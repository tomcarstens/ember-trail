import React, { useState } from "react";
import SlidingPanel from "react-sliding-side-panel";
import { Compendium } from "../components/compendium";

export const CompendiumContext = React.createContext({
    state: false,
    dispatch: () => null
})

export function CompendiumProvider(props) {
    const [state, dispatch] = useState(false);
    return <CompendiumContext.Provider value={{state, dispatch}}>
        {props.children}
      <SlidingPanel
        type={'right'}
        isOpen={state}
        size={30}
        backdropClicked={() => dispatch(false)}>
        <Compendium ></Compendium>
      </SlidingPanel>
    </CompendiumContext.Provider>
}